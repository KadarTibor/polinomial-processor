package PT2017.demo.Homework1.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import PT2017.demo.Homework1.Models.Polynomial;

public class DividePolynomialsTest {

	@Test
	public void test() {
		try{
			Polynomial p1 = new Polynomial("2x^2 + 4");
			Polynomial p2 = new Polynomial("1x^2 + 2");
			Polynomial[] results = new Polynomial[2];
			results = p1.dividePolynomials(p2);
			assertEquals("2",results[0].toString());
			assertEquals("0",results[1].toString());
		}
		catch(Exception e)
		{
			
		}

	
	}

}
