package PT2017.demo.Homework1.Models;

public class RealMonom extends Monom{
	//Real coefficient monomial's constructor
	RealMonom(int degree,Double coefficient){
		this.degree = degree;
		this.coefficient = coefficient;
	}
	//method for addition
	public Monom add(Monom other){
		  double rcoef = this.coefficient.doubleValue() + other.getCoefficient().doubleValue();
		   if(rcoef % 1 == 0) return  new IntegerMonom(this.degree,(int)rcoef);
		   else return new RealMonom(this.degree,rcoef);
	   }
	//method for subtraction
	public Monom sub(Monom other){
			 double rcoef = this.coefficient.doubleValue() - other.getCoefficient().doubleValue();
			   if(rcoef % 1 == 0) return  new IntegerMonom(this.degree,(int)rcoef);
			   else return new RealMonom(this.degree,rcoef);
		}
	//method for multiplication
	public Monom mul(Monom other){
			int rdegree = this.degree + other.getDegree();
			double rcoef = this.coefficient.doubleValue() * other.getCoefficient().doubleValue();
			if(rcoef % 1 == 0) return  new IntegerMonom(rdegree,(int)rcoef);
			   else return new RealMonom(rdegree,rcoef);
		}
	//method for division
	public Monom div(Monom other){

			int rdegree = this.degree - other.getDegree();
			double rcoef = this.coefficient.doubleValue() / other.getCoefficient().doubleValue();
			if(rcoef % 1 == 0) return  new IntegerMonom(rdegree,(int)rcoef);
			   else return new RealMonom(rdegree,rcoef);
		}
	//method for integration
	public Monom integrate(){
			int rdegree = this.degree + 1;
			double rcoef = this.coefficient.doubleValue() /(double)rdegree ;
			if(rcoef % 1 == 0) return  new IntegerMonom(rdegree,(int)rcoef);
			   else return new RealMonom(rdegree,rcoef);
	}
	//method for differentiation
	public Monom differentiate(){
		if(degree!=0){	
			int rdegree = this.degree - 1;
			double rcoef = this.coefficient.doubleValue() *degree ;
			if(rcoef % 1 == 0) return  new IntegerMonom(rdegree,(int)rcoef);
			else return new RealMonom(rdegree,rcoef);
		}
		else
		{
			return new IntegerMonom(0,0);
		}
	}
	public String toString(){
		if(degree == 0)
			return String.format("%.2f", coefficient);
		else
			return String.format("%.2fx^%d", coefficient,degree);
	}	
	

}
