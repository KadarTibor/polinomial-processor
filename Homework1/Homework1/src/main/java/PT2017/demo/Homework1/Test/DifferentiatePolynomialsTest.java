package PT2017.demo.Homework1.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import PT2017.demo.Homework1.Models.Polynomial;

public class DifferentiatePolynomialsTest {

	@Test
	public void test() {
		try{
			Polynomial p1 = new Polynomial("2x^2 + 1x^1 + 5");
			assertEquals("4x^1+1",p1.differentiatePolynomial().toString());
		}
		catch(Exception e)
		{
			
		}

	}

}
