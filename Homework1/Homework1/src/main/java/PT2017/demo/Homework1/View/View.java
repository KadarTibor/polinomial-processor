package PT2017.demo.Homework1.View;

import PT2017.demo.Homework1.Models.*;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class View {
   public Button add;
   public Button sub;
   public Button mul;
   public Button div;
   public Button differentiate;
   public Button integrate;
   public TextField p1textField;
   public TextField p2textField;
   public TextField result;
   public Scene scene;
   public Label l1;
   public Label l2;
   public Label l3;
   public Label l4;
   public Label l5;
   public Label l6;
   public Label errorLabel;
   public CheckBox checkBp1;
   public CheckBox checkBp2;
   public Pane pane;
   public TextField remainder;
   public TextField result1;
   
   public View() {
       
	   pane = new Pane();

	   scene = new Scene(pane, 400, 600);
	    
	   l1 = new Label();
	   l1.setText("Please introduce the 2 polynomials\n"
			+ "Input criterias:\n-x(lowercase)\n-non-negative powers\n-only the coef of x^0 is needed\n"
	   		+ "-(coefficient)x^(degree) ex: 1x^2 +2x^1 - 2\n"
	   		+ "Please push ENTER after you introduced the \npolynomial in order to load it!");
	   l1.setLayoutX(100);
	   l1.setLayoutY(50);
	   l1.setStyle("-fx-font-weight: bold");
	   pane.getChildren().add(l1);

	   l2 = new Label();
	   l2.setText("First polynomial");
	   l2.setStyle("-fx-font-weight: bold");
	   l2.setLayoutX(100);
	   l2.setLayoutY(200);
	   pane.getChildren().add(l2);
	   
	   l3  = new Label();
	   l3.setText("Second polynomial");
	   l3.setLayoutX(100);
	   l3.setStyle("-fx-font-weight: bold");
	   l3.setLayoutY(270);
	   pane.getChildren().add(l3);
	   
	   p1textField = new TextField();
	   p1textField.setLayoutX(100);
	   p1textField.setLayoutY(225);
	   p1textField.setPrefWidth(200);
	   pane.getChildren().add(p1textField);
	   
	   checkBp1 = new CheckBox();
	   checkBp1.setLayoutX(80);
	   checkBp1.setLayoutY(228);
	   pane.getChildren().add(checkBp1);
	   
	   p2textField = new TextField();
	   p2textField.setLayoutX(100);
	   p2textField.setLayoutY(295);
	   p2textField.setPrefWidth(200);
	   pane.getChildren().add(p2textField);
	   
	   checkBp2 = new CheckBox();
	   checkBp2.setLayoutX(80);
	   checkBp2.setLayoutY(298);
	   pane.getChildren().add(checkBp2);
	   
	   HBox hbox = new HBox();
	   hbox.setPadding(new Insets(10, 10, 10, 10));
	   hbox.setSpacing(10);
	   hbox.setLayoutX(87);
	   hbox.setLayoutY(330);
	   add = new Button("+");
	   add.setPrefSize(45, 25);
	   add.setDisable(true);
	   sub = new Button("-");
	   sub.setPrefSize(45, 25);
	   sub.setDisable(true);
	   div = new Button("/");
	   div.setPrefSize(45, 25);
	   div.setDisable(true);
	   mul = new Button("*");
	   mul.setPrefSize(45, 25);
	   mul.setDisable(true);
	   hbox.getChildren().addAll(add,sub,mul,div);
	   pane.getChildren().add(hbox);
	   
	   differentiate = new Button("D");
	   differentiate.setLayoutX(97);
	   differentiate.setLayoutY(370);
	   differentiate.setPrefSize(100, 25);
	   differentiate.setDisable(true);
	   pane.getChildren().add(differentiate);
	   
	   integrate = new Button("\u222B");
	   integrate.setLayoutX(207);
	   integrate.setLayoutY(370);
	   integrate.setPrefSize(100, 25);
	   integrate.setDisable(true);
	   pane.getChildren().add(integrate);
	   
	   l4 = new Label("Result");
	   l4.setLayoutX(100);
	   l4.setLayoutY(420);
	   l4.setStyle("-fx-font-weight: bold");
	   pane.getChildren().add(l4);
	   
	   result = new TextField();
	   result.setLayoutX(100);
	   result.setLayoutY(445);
	   result.setPrefWidth(200);
	   pane.getChildren().add(result);
	   
	   l5 = new Label("Remainder");
	   l5.setLayoutX(100);
	   l5.setLayoutY(485);
	   l5.setVisible(false);
	   l5.setStyle("-fx-font-weight: bold");
	   pane.getChildren().add(l5);
	   
	   remainder = new TextField();
	   remainder.setLayoutX(100);
	   remainder.setLayoutY(510);
	   remainder.setPrefWidth(200);
	   remainder.setVisible(false);
	   pane.getChildren().add(remainder);
	   
	   l6 = new Label("Result");
	   l6.setLayoutX(100);
	   l6.setLayoutY(485);
	   l6.setStyle("-fx-font-weight: bold");
	   l6.setVisible(false);
	   pane.getChildren().add(l6);
	   
	   result1 = new TextField();
	   result1.setLayoutX(100);
	   result1.setLayoutY(510);
	   result1.setPrefWidth(200);
	   result1.setVisible(false);
	   pane.getChildren().add(result1);
	   
	   
	    errorLabel = new Label("");
		errorLabel.setLayoutX(60);
		errorLabel.setStyle("-fx-font-weight: bold");
		errorLabel.setLayoutY(550);
		errorLabel.setTextFill(Color.web("#ecf0f1"));
		pane.getChildren().add(errorLabel);
		
		pane.setStyle("-fx-background-color:" + "#49A682");
   }
}
