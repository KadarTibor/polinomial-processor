package PT2017.demo.Homework1.Test;
import static org.junit.Assert.*;
import org.junit.Test;
import PT2017.demo.Homework1.Models.*;

public class AddPolynomialsTest {
   
	@Test
	public void addPolynomialTest(){
		try{
		Polynomial p1 = new Polynomial("1x^1 + 2");
		Polynomial p2 = new Polynomial("2x^2");
		System.out.println(p1.addPolynomials(p2)+" = " + "2x^2+1x^1+2");
		assertEquals("2x^2+1x^1+2",p1.addPolynomials(p2).toString());
		}
		catch (Exception e)
		{
			
		}
	}
	
}
