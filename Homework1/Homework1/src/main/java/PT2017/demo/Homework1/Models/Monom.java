package PT2017.demo.Homework1.Models;


/*
 * this class will model the concept of a monom
 */
abstract public class Monom implements Comparable<Monom>{
	
	protected int degree;//degree of the monom
	protected Number coefficient;//coefficient of the monom
	
	
	public int getDegree() {
		return degree;
	}
	
	public void setDegree(int degree) {
		this.degree = degree;
	}
	
	public Number getCoefficient() {
		return coefficient;
	}
	
	public void setCoefficient(Number coefficient) {
		this.coefficient = coefficient;
	}
	
	
	public int compareTo(Monom other){
			Integer i = new Integer(this.getDegree());
			return i.compareTo(other.getDegree())*(-1);
		
	}
	
	public abstract Monom add(Monom other);
	public abstract Monom sub(Monom other);
	public abstract Monom mul(Monom other);
	public abstract Monom div(Monom other);
	public abstract Monom integrate();
	public abstract Monom differentiate();
	
}

