package PT2017.demo.Homework1.View;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.stage.Stage;

import PT2017.demo.Homework1.Models.*;

public class PolynomViewTest extends Application  {
	
	public static void main(String[] args){
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Polynomial processing system");
		primaryStage.setResizable(false);
		View view = new View();
		Controller controller = new Controller(view);
		primaryStage.setScene(view.scene);
		primaryStage.show();
		
	}	
}
