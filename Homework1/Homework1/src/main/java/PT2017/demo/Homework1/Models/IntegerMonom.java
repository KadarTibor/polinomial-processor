package PT2017.demo.Homework1.Models;

public class IntegerMonom extends Monom {
   IntegerMonom(int degree, Integer coefficient){
	   this.degree = degree;
	   this.coefficient = coefficient;
   }
   
   public Monom add(Monom other){
	  double rcoef = this.coefficient.intValue() + other.getCoefficient().doubleValue();
	   if(rcoef % 1 == 0) return  new IntegerMonom(this.degree,(int)rcoef);
	   else return new RealMonom(this.degree,rcoef);
   }
	public Monom sub(Monom other){
		 double rcoef = this.coefficient.intValue() - other.getCoefficient().doubleValue();
		   if(rcoef % 1 == 0) return  new IntegerMonom(this.degree,(int)rcoef);
		   else return new RealMonom(this.degree,rcoef);
	}
	public Monom mul(Monom other){
		int rdegree = this.degree + other.getDegree();
		double rcoef = this.coefficient.intValue() * other.getCoefficient().doubleValue();
		if(rcoef % 1 == 0) return  new IntegerMonom(rdegree,(int)rcoef);
		   else return new RealMonom(rdegree,rcoef);
	}
	public Monom div(Monom other){

		int rdegree = this.degree - other.getDegree();
		double rcoef = this.coefficient.intValue() / other.getCoefficient().doubleValue();
		if(rcoef % 1 == 0) return  new IntegerMonom(rdegree,(int)rcoef);
		   else return new RealMonom(rdegree,rcoef);
	}
	public Monom integrate(){
		int rdegree = this.degree + 1;
		double rcoef = this.coefficient.intValue() /(double)rdegree ;
		if(rcoef % 1 == 0) return  new IntegerMonom(rdegree,(int)rcoef);
		   else return new RealMonom(rdegree,rcoef);
	}
	public Monom differentiate(){
		if(degree!=0){
		int rdegree = this.degree - 1;
		double rcoef = this.coefficient.intValue()*degree ;
		if(rcoef % 1 == 0) return  new IntegerMonom(rdegree,(int)rcoef);
		else return new RealMonom(rdegree,rcoef);
		}
		else return new IntegerMonom(0,0);	
		}
	public String toString(){
		if(degree == 0)	
			return String.format("%d", coefficient);
		else 
			return String.format("%dx^%d", coefficient, degree);
	}

}
