package PT2017.demo.Homework1.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import PT2017.demo.Homework1.Models.Polynomial;

public class MultiplyPolynomialsTest {

	@Test
	public void test() {
		try{
			Polynomial p1 = new Polynomial("2x^2 + 1x^1 + 5");
			Polynomial p2 = new Polynomial("1x^1 + 3");
			assertEquals("2x^3+7x^2+8x^1+15",p1.multiplyPolynomials(p2).toString());
		}
		catch(Exception e)
		{
			
		}

	}

}
