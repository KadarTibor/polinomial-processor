package PT2017.demo.Homework1.Models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/*
 * This class will model the concept of polynomials
 */

public class Polynomial {
	
	//we need a list of Monoms
	private List<Monom> monoms = new ArrayList<Monom>();
	
	public Polynomial(String s) throws Exception{
		try{
			createPolynomial(s);
			Collections.sort(monoms);
			for(Monom m : monoms)
			{
				if(m.getDegree()<0) throw new Exception();
			}
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	public Polynomial(List<Monom> monoms){
		this.monoms = monoms;
		Collections.sort(monoms);
	}
	
	private void createPolynomial(String s){
		//process the given string and set up the monoms
		int coefficient = 0;
		int degree = 0;
		
		s = s.replace(" -", " +-");
		String[] ms = s.split("\\+");
		
		for(int i = 0; i < ms.length; i++){
				ms[i] = ms[i].trim();
				
				if(ms[i].contains("x")){
					String[] mNr = ms[i].split("x");
					if(mNr[0]=="") {
							coefficient = 1;
						}
					else {
							
							coefficient = Integer.parseInt(mNr[0]);
						}
					mNr[1] = mNr[1].trim();
					degree = Integer.parseInt(mNr[1].substring(mNr[1].indexOf("^")+1));
				}
				else {//this will treat the possible x^0 case  
					if(!ms[i].equals("")){
						coefficient = Integer.parseInt(ms[i]);
						degree  = 0;
					}	  
				}
				//create and add the monom
				if(coefficient != 0){
					Monom a = new IntegerMonom(degree,coefficient);
					boolean introduce = true;
					for(Monom m: monoms){
						if(a.getDegree()==m.getDegree()){
						    introduce = false;
							m = m.add(a);
						}
					}
					if(introduce) monoms.add(a);
				}
			
				coefficient = 0;
			
		}
	}	
			
	public String toString(){
		String a = "";
		if(monoms.isEmpty()) 
			return "0";
		for(Monom temp: monoms){
			if(temp == monoms.get(0) && temp.getCoefficient().doubleValue()>0)	
				a += temp.toString();
			else if(temp.getCoefficient().doubleValue()==0)
				  a+="";
			else if(temp.getCoefficient().doubleValue() <0)
				     a +=temp.toString();
			else if (temp.getCoefficient().doubleValue() >0)
					a += "+" + temp.toString();
		}
		return a;
		}

			
    //method that enables access to the monoms
	public List<Monom> getMonoms()
	{
		return monoms;
	}
	
	//addition
		public Polynomial addPolynomials(Polynomial pol2){
			List<Monom> pol1Monoms = this.getMonoms();
			List<Monom> pol2Monoms = pol2.getMonoms();
			List<Monom> rezMonoms = new ArrayList<Monom>();
			boolean foundTerm;
			//add the terms with the same degree and the terms from pol1 that have no match in pol2
			for(Monom p1monom: pol1Monoms){
				foundTerm = false;
				for(Monom p2monom: pol2Monoms){
					if(p1monom.getDegree()==p2monom.getDegree()){
						Monom a = p1monom.add(p2monom);
						rezMonoms.add(a);
						foundTerm = true;
					}
				}
				if(!foundTerm)
					rezMonoms.add(p1monom);
			}
			//add the terms from pol2 that have no match in pol1
			for(Monom p2monom: pol2Monoms){
				foundTerm = false;
				for(Monom p1monom: pol1Monoms){
					if(p1monom.getDegree()==p2monom.getDegree()){
						foundTerm = true;
					}
				}
				if(!foundTerm)
					rezMonoms.add(p2monom);
			}
			Polynomial result = new Polynomial(rezMonoms);
			return result;
		}	
		//substraction
		public Polynomial substrPolynomials(Polynomial pol2){
			List<Monom> pol1Monoms = this.getMonoms();
			List<Monom> pol2Monoms = pol2.getMonoms();
			List<Monom> rezMonoms = new ArrayList<Monom>();
			boolean foundTerm;
			//add the terms with the same degree and the terms from pol1 that have not match in pol2
			for(Monom p1monom: pol1Monoms){
				foundTerm = false;
				for(Monom p2monom: pol2Monoms){
					if(p1monom.getDegree()==p2monom.getDegree()){
						Monom a = p1monom.sub(p2monom);
						rezMonoms.add(a);
						foundTerm = true;
					}
				}
				if(!foundTerm)
					rezMonoms.add(p1monom);
			}
			//add the terms from pol2 that have no match in pol1
			for(Monom p2monom: pol2Monoms){
				foundTerm = false;
				for(Monom p1monom: pol1Monoms){
					if(p1monom.getDegree()==p2monom.getDegree()){
						foundTerm = true;
					}
				}
				if(!foundTerm){
				   Monom element  = p2monom.mul(new IntegerMonom(0,-1));
					rezMonoms.add(element);
					
				}
			}
			Polynomial result = new Polynomial(rezMonoms);
			return result;
		}
		
		//multiply polynomials
		public Polynomial multiplyPolynomials(Polynomial pol2){
			
			List<Monom> pol1Monoms = this.getMonoms();
			List<Monom> pol2Monoms = pol2.getMonoms();
			List<Monom> rezMonoms = new ArrayList<Monom>();
			for(Monom p1m: pol1Monoms){
				for(Monom p2m: pol2Monoms){
					//multiply the two terms and create the new Monom
					Monom a = p1m.mul(p2m);
					for(Monom temp: rezMonoms){
						if(temp.getDegree()==a.getDegree()){
							 a = temp.add(a);
							 rezMonoms.remove(temp);//remove the other term from the list becouse we updated it
							 break;
						}
					}
				if(a.coefficient.doubleValue()!=0)	
				rezMonoms.add(a); 	
				}
			}		
			Polynomial result = new Polynomial(rezMonoms);
			return result;
		
		}
		
		//division
		public Polynomial[] dividePolynomials( Polynomial divider){
			List<Monom> dividents = this.getMonoms();
			Polynomial Divident = new Polynomial(dividents);
			List<Monom> dividers = divider.getMonoms();
			List<Monom> results = new ArrayList<Monom>();
			List<Monom> oneElementPolynomial = new ArrayList<Monom>();
			List<Monom> container = new ArrayList<Monom>();
			int degree = dividents.get(0).getDegree();
			int dividersDegree = dividers.get(0).getDegree();
			
			while(degree >= dividersDegree ){
				Monom partialResult =dividents.get(0).div(dividers.get(0));
				results.add(partialResult);
			    oneElementPolynomial.add(partialResult);
			    Polynomial oneElement = new Polynomial(oneElementPolynomial);
			    Polynomial scadent = oneElement.multiplyPolynomials(divider);
			    Divident = Divident.substrPolynomials( scadent);
			    dividents = Divident.getMonoms();
			     oneElementPolynomial.clear();
			     for(Monom temp:dividents){
			    	 if(temp.getCoefficient().doubleValue()!=0){
			    		container.add(temp);
			    	 }	 
			     }
			     dividents.clear();
			     for(Monom temp:container){
			    	 dividents.add(temp); 
			     }
			     container.clear();
			     if(!Divident.getMonoms().isEmpty())
			    	 degree = dividents.get(0).getDegree();
			     else 
			    	 degree = -1;
			}
			Polynomial[] result = new Polynomial[2];
			result[0] = new Polynomial(results);
			result[1] = Divident;
			return result;
		}
		//differentiation
		public Polynomial differentiatePolynomial(){
			List<Monom> pol1Monoms = this.getMonoms();
			List<Monom> rezMonoms = new ArrayList<Monom>();
			
			for(Monom temp: pol1Monoms){
				Monom a = temp.differentiate();
				if(a.coefficient.doubleValue()!=0){
				rezMonoms.add(a);
				}
			}
			return new Polynomial(rezMonoms);
		}
		
		//Integration
		public Polynomial integratePolynomial(){
			List<Monom> pol1Monoms = this.getMonoms();
			List<Monom> rezMonoms = new ArrayList<Monom>();
			
			for(Monom temp: pol1Monoms){
				Monom a = temp.integrate();
				rezMonoms.add(a);
			}
			Polynomial result = new Polynomial(rezMonoms);
			return result;
		}

}
	


