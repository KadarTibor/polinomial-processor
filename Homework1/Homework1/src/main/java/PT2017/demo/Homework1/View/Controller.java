package PT2017.demo.Homework1.View;
import PT2017.demo.Homework1.Models.*;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;

public class Controller implements EventHandler<ActionEvent>{

	public View view;
	public Polynomial[] result = new Polynomial[2];
	public Polynomial p1;
	public Polynomial p2;
	
	
	Controller(View view){
		//set the actionhandler for the buttons and checkboxes and textfields
		this.view = view;
		view.p1textField.setOnAction(this);
		view.p2textField.setOnAction(this);
		view.checkBp1.setOnAction(this);
		view.checkBp2.setOnAction(this);
		view.add.setOnAction(this);
		view.sub.setOnAction(this);
		view.mul.setOnAction(this);
		view.div.setOnAction(this);
		view.differentiate.setOnAction(this);
		view.integrate.setOnAction(this);
	}
	
	public void handle(ActionEvent event) {
		//handle the first polynomial text field
		if(event.getSource()==view.p1textField){
			clearer();
			try{
				p1 = null;
				if(!view.p1textField.getText().equals("")){
					p1 = new Polynomial(view.p1textField.getText());
					view.errorLabel.setText("");
					if(p1 != null && p2 != null){
						view.add.setDisable(false);
						view.sub.setDisable(false);
						view.mul.setDisable(false);
						if(!view.p1textField.getText().trim().equals("0") && !view.p2textField.getText().trim().equals("0"))
							view.div.setDisable(false);
						else 
							view.div.setDisable(true);
				}
				}
				else{//hide buttons if there are not 2 polynomials
					view.add.setDisable(true);
					view.sub.setDisable(true);
					view.mul.setDisable(true);
					view.div.setDisable(true);
				}
			
			}
			catch(Exception E)
			{
				if(view.errorLabel.getText().equals("")){
					view.errorLabel.setText("The first polynomial does not match the input criterias!");
				}
				else
					view.errorLabel.setText("  Neither of the polynomials match the input criteria!");
				p1 = null;
				//hide the buttons if the polynomial was wrongly input
				view.add.setDisable(true);
				view.sub.setDisable(true);
				view.mul.setDisable(true);
				view.div.setDisable(true);
			}
		}
		//handle the second polynomial text field 
		if(event.getSource()==view.p2textField){
			clearer();
			try{
				p2 = null;
				if(!view.p2textField.getText().equals("")){
				p2 = new Polynomial(view.p2textField.getText());
				view.errorLabel.setText("");
				if(p1 != null && p2 != null){
					view.add.setDisable(false);
					view.sub.setDisable(false);
					view.mul.setDisable(false);
					if(!view.p2textField.getText().trim().equals("0") && !view.p1textField.getText().trim().equals("0"))
						view.div.setDisable(false);
					else view.div.setDisable(true);
				}
				}
				else
				{
					view.add.setDisable(true);
					view.sub.setDisable(true);
					view.mul.setDisable(true);
					view.div.setDisable(true);
				}
			}
			catch(Exception E)
			{
				if(view.errorLabel.getText().equals("")){
					view.errorLabel.setText("The second polynomial does not match the input criterias!");
				}
				else
					view.errorLabel.setText("Neither of the polynomials match the input criteria");
				p2 = null;
				view.add.setDisable(true);
				view.sub.setDisable(true);
				view.mul.setDisable(true);
				view.div.setDisable(true);
				
			}
		}
		//handle the first checkbox
		if(event.getSource()==view.checkBp1){
			clearer();
			if((p1 != null || p2 != null) && view.checkBp1.isSelected())
			{
				view.differentiate.setDisable(false);
				view.integrate.setDisable(false);
			}
			else
			  if(!view.checkBp2.isSelected()){
				view.differentiate.setDisable(true);
				view.integrate.setDisable(true);
			}
			}
		
		//handle the second checkbox
		if(event.getSource()==view.checkBp2){
			clearer();
			if((p1 != null || p2 != null) && view.checkBp2.isSelected())
			{
				view.differentiate.setDisable(false);
				view.integrate.setDisable(false);
			}
			else
			if(!view.checkBp1.isSelected()){
				view.differentiate.setDisable(true);
				view.integrate.setDisable(true);
			}
		}
		
		//handle the add button
		if(event.getSource()== view.add){
			clearer();
			result[0] = null;
			result[0] = p1.addPolynomials(p2);
			view.result.setText(result[0].toString());
		}
		
		//handle the sub button
		if(event.getSource()== view.sub){
			clearer();
			result[0] = null;
			result[0] = p1.substrPolynomials(p2);
			view.result.setText(result[0].toString());
		}
		
		//handle the multiply button
		if(event.getSource()== view.mul){
			clearer();
			result[0] = p1.multiplyPolynomials(p2);
			view.result.setText(result[0].toString());
		}
		
		//handle the division button
		if(event.getSource()== view.div){
			clearer();
			result = p1.dividePolynomials(p2);
			view.result.setText(result[0].toString());
			view.remainder.setText(result[1].toString());
			view.remainder.setVisible(true);
			view.l5.setVisible(true);
			
		}
		
		//handle the differentiation
		if(event.getSource()== view.differentiate){
			clearer();
			if(view.checkBp1.isSelected()){
				result[0] = p1.differentiatePolynomial();
				view.result.setText(result[0].toString());
			}
			if(view.checkBp2.isSelected() && view.checkBp1.isSelected()){
				result[1] = p2.differentiatePolynomial();
				view.l6.setVisible(true);
				view.result1.setVisible(true);
				view.result1.setText(result[1].toString());
			}
			if(view.checkBp2.isSelected() && !view.checkBp1.isSelected())
			{
				result[0] = p2.differentiatePolynomial();
				view.result.setText(result[0].toString());
			}
			
		}
		

		//handle the differentiation
		if(event.getSource()== view.integrate){
			clearer();
			if(view.checkBp1.isSelected()){
				result[0] = p1.integratePolynomial();
				view.result.setText(result[0].toString());
			}
			if(view.checkBp2.isSelected() && view.checkBp1.isSelected()){
				result[1] = p2.integratePolynomial();
				view.l6.setVisible(true);
				view.result1.setVisible(true);
				view.result1.setText(result[1].toString());
			}
			if(view.checkBp2.isSelected() && !view.checkBp1.isSelected())
			{
				result[0] = p2.integratePolynomial();
				view.result.setText(result[0].toString());
			}
		}
		
	}
	//reinitialise the window with this method in order to hide the two additional textfields
	private void clearer()
	{
		view.remainder.setVisible(false);
		view.l5.setVisible(false);
		view.l6.setVisible(false);
		view.result1.setVisible(false);
		
	}
		
}


