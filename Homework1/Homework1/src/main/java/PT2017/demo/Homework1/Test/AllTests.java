package PT2017.demo.Homework1.Test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AddPolynomialsTest.class, DifferentiatePolynomialsTest.class, DividePolynomialsTest.class,
		IntegratePolynomialTest.class, MultiplyPolynomialsTest.class, SubstractPolynomialsTest.class })
public class AllTests {

}
